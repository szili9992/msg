package WorkingWithInheritance;

/**
 * Created by szili on 2016-06-21.
 */
public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return 3.14 * Math.pow(2, getRadius()) * getHeight();
    }

    /*
    * Overridden getArea(); The exercise suggests that after I create the overridden getArea() method the getVolume method won't work anymore.
    * However we calculate the cylinders volume with pi*radius*height. The getArea() method won't affect the getVolume() method.
    * */
    public double getArea() {
        return (2 * 3.14 * getRadius() * getHeight()) + (2 * super.getArea());
    }

    @Override
    public String toString() {
        return "Cylinder{" +
                "height=" + height +
                '}';
    }
}
