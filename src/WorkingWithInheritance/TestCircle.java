package WorkingWithInheritance;

/**
 * Created by szili on 2016-06-21.
 */
//Because the variable radius in Circle class is set private other classes can't access it other then the class where it was declared.

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        c1.setColor("green");
        c1.setRadius(2.4);
        System.out.println("the circle has a radius of " + c1.getRadius() + " area of " + c1.getArea() + " and it's " + c1.getColor());


        Circle c2 = new Circle(2.0);
        c2.setColor("magenta");
        c2.setRadius(3.14);
        System.out.println("the circle has a radius of " + c2.getRadius() + " area of " + c2.getArea() + " and it's " + c2.getColor());
    }
}
