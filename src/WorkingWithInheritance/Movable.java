package WorkingWithInheritance;

/**
 * Created by szili on 2016-06-22.
 */
public interface Movable {
    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();
}
