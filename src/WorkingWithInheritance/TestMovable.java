package WorkingWithInheritance;

/**
 * Created by szili on 2016-06-22.
 */
public class TestMovable {
    public static void main(String[] args) {
        MovablePoint movablePoint = new MovablePoint(5, 6);
        System.out.println("point one: " + movablePoint);
        movablePoint.moveDown();
        System.out.println("point one: " + movablePoint);
        movablePoint.moveRight();
        System.out.println("point one: " + movablePoint);

        MovablePoint point = new MovablePoint(6, 2);
        System.out.println("point two: " + point);
        point.moveUp();
        System.out.println("point two: " + point);
        point.moveLeft();
        System.out.println("point two: " + point);
    }
}
