package WorkingWithInheritance;

/**
 * Created by szili on 2016-06-18.
 * Working with inheritance
 */
public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    /*
    * third constructor for constructing circle instance with given radius and color
    * */
    public Circle(double radius, String color) {  //
        this.radius = radius;
        this.color = color;
    }

    /*
    * add a getter for variable color then test it
    * */
    public String getColor() {
        return this.color;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {
        return 3.14 * Math.pow(getRadius(), 2);
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
