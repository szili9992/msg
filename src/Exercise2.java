/**
 * Created by szili on 2016-06-20.
 * 9. Examine the following program and complete the program so that a new array "twice" in constructed. Now copy values form "val"
 * to "twice" but make the values in "twice" double what they are in val.
 */
public class Exercise2 {
    public static void main(String[] args) {
        int[] val = {13, -4, 82, 17};
        int[] twice;
        System.out.println("Original array: " + val[0] + " " + val[1] + " " + val[2] + " " + val[3]);
        twice = new int[val.length];
        for (int i = 0; i < val.length; i++)
            twice[i] = 2 * val[i];
        System.out.println("New: " + twice[0] + " " + twice[1] + " " + twice[2] + " " + twice[3]);

    }
}
