/**
 * Created by szili on 2016-06-18.
 * 7. Is the city a metropolis(capital city)
 */
public class City {
    private boolean isCapitalCity;
    private int numberOfCitizen, taxPerCitizen;

    public City() {
    }

    private boolean isCapitalCity() {
        return isCapitalCity;
    }

    private void setCapitalCity(boolean capitalCity) {
        isCapitalCity = capitalCity;
    }

    public int getNumberOfCitizen() {
        return numberOfCitizen;
    }

    public void setNumberOfCitizen(int numberOfCitizen) {
        this.numberOfCitizen = numberOfCitizen;
    }

    public int getTaxPerCitizen() {
        return taxPerCitizen;
    }

    public void setTaxPerCitizen(int taxPerCitizen) {
        this.taxPerCitizen = taxPerCitizen;
    }

    public void isItACapitalCity() {
        if (getNumberOfCitizen() > 1000000 && getTaxPerCitizen() > 720000000)
            setCapitalCity(true);
        else
            setCapitalCity(false);

        System.out.println("This city is a Metropolis: " + isCapitalCity());
    }

    @Override
    public String toString() {
        return "City{" +
                "isCapitalCity=" + isCapitalCity +
                ", numberOfCitizen=" + numberOfCitizen +
                ", taxPerCitizen=" + taxPerCitizen +
                '}';
    }
}
