import java.util.Scanner;

/**
 * Created by szili on 2016-06-14.
 * 4.  Write a program to work out if a series of five digits are consecutive numbers.
 * There are two methods I used to complete this task.
 */
public class ConsecutiveNumbers {
    public static void main(String[] args) {
        String numbers1 = "5-6-7-8-9";
        String numbers2 = "7-8-9-10-12";

        System.out.println("The numbers are in order: " + isConsecutiveOne(numbers1));
        System.out.println("The numbers are in order: " + isConsecutiveTwo(numbers2));
    }

    /*
    * Method number one. Can only tell if the numbers are single digits.
    * */
    static boolean isConsecutiveOne(String numbers) {
        int currentNumber;
        int lastNumber = new Scanner(numbers).useDelimiter("\\D+").nextInt() - 1;

        for (int i = 0; i < numbers.length(); i++) {
            if (Character.isDigit(numbers.charAt(i))) {
                currentNumber = Integer.parseInt(String.valueOf(numbers.charAt(i)));
                if (currentNumber == (lastNumber + 1)) {
                    lastNumber = currentNumber;
                } else
                    return false;
            }
        }
        return true;
    }

    /*
    * Method number two. Can go beyond one digit.
    * */
    static boolean isConsecutiveTwo(String numbers) {
        int currentNumber;
        int lastNumber;
        String[] splitNumbers = numbers.split("-");

        for (int i = 1; i < splitNumbers.length; i++) {
            lastNumber = Integer.parseInt(String.valueOf(splitNumbers[i - 1]));
            currentNumber = Integer.parseInt(String.valueOf(splitNumbers[i]));
            if (currentNumber != (lastNumber + 1)) {
                return false;
            }
        }
        return true;
    }
}
