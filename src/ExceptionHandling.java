import java.lang.reflect.Array;

/**
 * Created by szili on 2016-07-06.
 */
//second exercise
class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}

//third exercise
class Throwing {
    public void throwFirst() {
        //throw new MyException("Throwing class");
        //compiler gives unreported exception MyException; must be caught or declared to be thrown
    }

    public void throwSecond() throws MyException {
        throw new MyException("Throwing class");
    }
}

//fifth exercise
class Five {
    public void g() throws Exception {
        throw new Exception("Out of milk");
    }

    public void f() throws Exception {
        try {
            g();
        } catch (Exception e) {
            throw new Exception("Out of bread");
        }
    }
}

//sixth exercise
class Ex1 extends Exception {
    public Ex1(String s) {
        super(s);
    }
}

class Ex2 extends Exception {
    public Ex2(String s) {
        super(s);
    }
}

class Ex3 extends Exception {
    public Ex3(String s) {
        super(s);
    }
}

class Six {
    public void f() throws Ex1, Ex2, Ex3 {
        throw new Ex1("Method can only throw one exception at a time");
    }
}

public class ExceptionHandling {
    public static void main(String[] args) {
        //first exercise
        try {
            throw new Exception("this is an exception from first exercise");
        } catch (Exception e) {
            System.err.println("one: " + e.getMessage());
        } finally {
            System.out.println("I'm printing this anyway");
        }
        //second exercise
        try {
            throw new MyException("second exercise");
        } catch (MyException e) {
            System.err.println("two: " + e.getMessage());
        }
        //third exercise
        Throwing throwing = new Throwing();
        try {
            throwing.throwSecond();
        } catch (MyException e) {
            System.err.println("three: " + e.getMessage());
        }
        //fourth exercise
        Throwing t = null;
        try {
            t.throwSecond();
        } catch (Exception e) {
            System.err.println("four: " + e.getMessage());
        }
        //fifth exercise
        Five five = new Five();
        try {
            five.f();
        } catch (Exception e) {
            System.err.println("five: " + e.getMessage());
        }
        //sixth exercise
        Six six = new Six();
        try {
            six.f();
        } catch (Ex1 | Ex2 | Ex3 e) {
            System.err.println("six: " + e.getMessage());
        }
        //seventh exercise
        String[] strings = {"Apple", "Donald", "Width"};
        try {
            System.out.println(strings[4]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("seven: " + e.getMessage());
        } finally {
            System.out.println("End of the chapter");
        }
    }
}
