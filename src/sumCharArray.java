/**
 * Created by szili on 2016-06-18.
 * 6. Implement a java function that calculates the sum of digits for a given char array consisting of the digits 0-9. The function should return the sum as long value.
 */
public class SumCharArray {
    public static void main(String[] args) {

        char[] digits = {'7', '5', '7', '3', '2', '6', 'h'};

        System.out.println("sum of the digits: " + calcSum(digits));
    }

    public static long calcSum(char[] digits) {
        int sum = 0;
        for (char digit : digits) {
            if (Character.isDigit(digit)) {
                int currentNumber = Integer.parseInt(String.valueOf(digit));
                sum += currentNumber;
            }
        }
        return sum;
    }
}
