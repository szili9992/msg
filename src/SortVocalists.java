/**
 * Created by szili on 2016-06-13.
 * 1. Write a program to loop around and count how many male/female vocalists are in the array.
 */
public class SortVocalists {
    public static void main(String args[]) {
        int sumF = 0;
        int sumM = 0;
        String vocalists[] = new String[]{"Beyonce(f)", "David Bowie(m)", "Elvis Costello(m)", "Madonna(f)", "Elton John(m)", "Charles Aznavour(m)"};

        for (String vocalist : vocalists) {
            if (vocalist.charAt(vocalist.length() - 2) == 'f') {
                sumF += 1;
            } else if (vocalist.charAt(vocalist.length() - 2) == 'm')
                sumM += 1;
        }
        System.out.println("Number of female vocalists: " + sumF);
        System.out.println("Number of male vocalists: " + sumM);
    }
}