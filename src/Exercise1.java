/**
 * Created by szili on 2016-06-20.
 * 8. Examine the following program and complete the assignment statement so that it computes the sum of the numbers in the array
 */
public class Exercise1 {
    public static void main(String[] args) {
        int[] val = {0, 1, 2, 3};
        int sum = 0;
        for (int i = 0; i < val.length; i++)
            sum += val[i];
        System.out.println("Sum of all numbers: " + sum);
    }
}
