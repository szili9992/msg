import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by szili on 2016-06-16.
 * 5. Write a programme to work out how many wins Manchester United had, how many games they drew, and how many Manchester United lost.
 * Extend the programme to work out how many goals Manchester United scored and how many they conceded.
 * Suppose a win gains you 3 points, a draw 1 point, and a loss no points. Have your programme work out how many points in total Manchester United have acquired.
 */
public class Manchester {
    public static void main(String[] args) {
        String line = "Manchester United 1 Chelsea 0, Arsenal 1 Manchester United 1, Manchester United 3 Fulham 1, Liverpool 2 Manchester United 1, Swansea 2 Manchester United 4";
        line = line.replace(",", "");
        String pattern = "(\\D)+ (\\d)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line);
        int counter = 0;
        while (m.find()) {
            counter++;
            System.out.println("Csapat: " + m.group().trim().replaceAll("[0-9]", "").trim() + ", gol: " + m.group().replaceAll("[^0-9]", ""));
            if (counter % 2 == 0) {
                System.out.println("");
            }

        }
    }
}
