import java.util.Arrays;

/**
 * Created by szili on 2016-06-20.
 * 10. Write a program and use overloaded methods for printing different types of array(integer, double, char)
 */
public class OverloadedMethods {
    public static void main(String[] args) {
        int[] integers={1,2,3,4,5,6};
        double[] doubles={5.77,3.22,8.11,9.58};
        char[] chars={'s','o',' ','m','u','c','h',' ','w','o','w'};
        printArray(integers);
        printArray(doubles);
        printArray(chars);

    }
    public static int printArray(int[] array){
        System.out.println(Arrays.toString(array));
        return 0;
    }
    public static double printArray(double[] array){
        System.out.println(Arrays.toString(array));
        return 0;
    }
    public static char printArray(char[] array){
        System.out.println(Arrays.toString(array));
        return 0;
    }
}
