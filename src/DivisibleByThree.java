import java.util.stream.IntStream;

/**
 * Created by szili on 2016-06-13.
 * 3. Write a program to count how many numbers between 1 and 1000 are divisible by 3 with no remainder.
 */
public class DivisibleByThree {
    public static void main(String args[]) {
        int sum = 0;
        int[] ranges = IntStream.rangeClosed(1, 1000).toArray();
        for (int range : ranges)
            if (range % 3 == 0)
                sum += 1;

        System.out.println("Number of numbers that are divisible by 3 with no remainder: " + sum);
    }
}
