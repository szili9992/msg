/**
 * Created by szili on 2016-06-13.
 * 2. Loop around and remove the vowels.
 */
public class InstrumentVowels {
    public static void main(String args[]) {
        String[] instruments = new String[]{"cello", "guitar", "violin", "double bass"};
        System.out.println("Instruments without vowels: ");
        for (String instrument : instruments) {
            instrument = instrument.replaceAll("[aeiou]", "");
            System.out.println(instrument);
        }
    }
}
