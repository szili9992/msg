import java.util.Arrays;
import java.util.Random;

/**
 * Created by szili on 2016-06-18.
 * Flow control
 */
public class FlowControl {
    public static void main(String[] args) {
        numberFive();
    }


    /*
    * Write program that prints values from 1 to 100;
    * Modify so the program exits by using the break keyword at value 47. Try using return instead;
    * */
    public void numberOneTwo() {
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
            if (i == 47)
                return;
        }
    }

    /*
    * Write a program that generates 25 random int values. For each value use an if-else statement to classify it as greater/less/equal to a second randomly generated value.
    * */
    public static void numberThree() {
        Random r = new Random();
        int[] randomNumbers = r.ints(25, 0, 11).toArray();
        System.out.println(Arrays.toString(randomNumbers));

        for (int i = 0; i < randomNumbers.length - 1; i++) {
            if (randomNumbers[i] < randomNumbers[i + 1])
                System.out.println(randomNumbers[i] + " is less than " + randomNumbers[i + 1]);
            else if (randomNumbers[i] == randomNumbers[i + 1])
                System.out.println(randomNumbers[i] + " is equal to " + randomNumbers[i + 1]);
            else if (randomNumbers[i] > randomNumbers[i + 1])
                System.out.println(randomNumbers[i] + " is greater then " + randomNumbers[i + 1]);

        }
    }

    /*
    * Modify exercise 3 so that the code is surrounded by an infinite while loop. Interrupt it from keyboard.
    * */
    public static void numberFour() {
        Random r = new Random();
        int[] randomNumbers = r.ints(25, 0, 11).toArray();
        System.out.println(Arrays.toString(randomNumbers));


        while (1 == 1) {
            for (int i = 0; i < randomNumbers.length - 1; i++) {
                if (randomNumbers[i] < randomNumbers[i + 1])
                    System.out.println(randomNumbers[i] + " is less than " + randomNumbers[i + 1]);
                else if (randomNumbers[i] == randomNumbers[i + 1])
                    System.out.println(randomNumbers[i] + " is equal to " + randomNumbers[i + 1]);
                else if (randomNumbers[i] > randomNumbers[i + 1])
                    System.out.println(randomNumbers[i] + " is greater then " + randomNumbers[i + 1]);
            }
        }

    }

    /*
    * Create a switch statement that prints a message for each case , and put the switch in a for loop that tries each case. Put a break after each case and test it then remove the breaks and see what happens.
    * */
    public static void numberFive() {

        for (int i = 1; i <= 6; ++i) {
            switch (i) {
                case 1:
                    System.out.println("Your coding is bad and you should feel bad");

                case 2:
                    System.out.println("I got 99 problems but a glitch ain't one");

                case 3:
                    System.out.println("My code works, and I don't know why");
                    break;
                case 4:
                    System.out.println("My code doesn't work, and I don't know why");
                    break;
                case 5:
                    System.out.println("Much coding");

                case 6:
                    System.out.println("Many errors");

            }
        }
    }

}
